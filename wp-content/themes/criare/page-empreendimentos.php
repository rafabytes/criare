<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Empreendimentos */
get_header();
?>

<div id="primary" class="content-area">
    <main id="empreendimentos" class="site-page">

        <section class="page-header">
            <a href="<?php echo home_url('sobre'); ?>" class="animsition-link arrow arrow-black prev v-middle">Sobre</a>
            <h1><?php the_field('titulo') ?></h1>
            <h5 class="subtitle"><?php the_field('titulo_filtros') ?></h5>
            <div class="filtros d-flex align-items-end justify-content-middle">
                <div class="col pr-0">
                    <div class="filtro">
                        <h5 class="label">Tipo</h5>
                        <select class="custom-select" name="tipo" id="tipo">
                            <?php
                            $tipos = get_terms(
                                array(
                                    'taxonomy'   => 'tipo',
                                    'hide_empty' => true,
                                    'orderby' => 'name',
                                    'order' => 'ASC'
                                )
                            );

                            if (!empty($tipos) && is_array($tipos)) {
                                foreach ($tipos as $tipo) : ?>
                                    <option value="<?php echo $tipo->slug; ?>" <?php if ($para === $tipo->slug) {
                                                                                    echo 'selected';
                                                                                } ?>><?php echo $tipo->name; ?></option>
                            <?php
                                endforeach;
                            }
                            ?>
                            <option value="">VER TODOS</option>
                        </select>
                    </div>
                </div>
                <div class="col pr-0">
                    <div class="filtro">
                        <h5 class="label">Fase da Obra</h5>
                        <select class="custom-select" name="tipo" id="tipo">
                            <?php
                            $fases = get_terms(
                                array(
                                    'taxonomy'   => 'fase',
                                    'hide_empty' => true,
                                    'orderby' => 'name',
                                    'order' => 'ASC'
                                )
                            );

                            if (!empty($fases) && is_array($fases)) {
                                foreach ($fases as $fase) : ?>
                                    <option value="<?php echo $fase->slug; ?>" <?php if ($para === $fase->slug) {
                                                                                    echo 'selected';
                                                                                } ?>><?php echo $fase->name; ?></option>
                            <?php
                                endforeach;
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-auto">
                    <a href="#" class="btn outline">Filtrar</a>
                </div>
            </div>
            <a href="<?php echo home_url('/empreendimentos'); ?>" class="animsition-link arrow arrow-black next v-middle">Empreendimentos</a>
        </section>

        <section class="imoveis">
            <div class="container">
                <div id="loop-content" data-tipo data-fase class="row">
                    <?php
                    $imoveis = new WP_Query(
                        array(
                            'post_type' => 'empreendimento',
                            // 'tax_query'    => array(
                            //     'relation'        => 'AND',
                            //     array(
                            //         'taxonomy'    => 'status',
                            //         'field'        => 'slug',
                            //         'terms'          => array('portfolio'),
                            //         'operator' => 'NOT IN'
                            //     )
                            // ),
                            'posts_per_page' => -1
                        )
                    );
                    while ($imoveis->have_posts()) :  $imoveis->the_post();
                    ?>
                        <div class="col-md-6">
                            <div class="imovel">
                                <div class="meta">
                                    <h2><?php the_title(); ?></h2>
                                    <div class="local"><?php the_field('cidade'); ?></div>
                                </div>
                                <div class="owl-carousel owl-imovel">
                                    <?php
                                    $fotos = acf_photo_gallery('fotos', get_the_ID());
                                    foreach ($fotos as $image) :
                                    ?>
                                        <div class="item">
                                            <a href="<?php the_permalink() ?>">
                                                <div class="img" style="background-image: url(<?php echo $image['full_image_url'] ?>);"></div>
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </section>

    </main>
</div>


<?php
get_footer();
