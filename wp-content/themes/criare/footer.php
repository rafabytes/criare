<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

$home = get_page_by_path('homepage');

?>

</div><!-- #content -->

<footer id="mainFooter" class="site-footer">
	<div class="container px-0">
		<div class="row align-items-center">
			<div class="logo col">
				<img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" alt="Criare">
			</div>
			<div class="copyright col text-center">@2021 Criare - Todos os Direitos Reservados</div>
			<div class="site-info col text-right">
				<a href="tel:<?php the_field('telefone',$home->ID) ?>" class="fone"><?php the_field('telefone',$home->ID) ?></a>
				<ul class="social">
					<li class="facebook"><a href="<?php the_field('facebook',$home->ID) ?>" target="_blank"></a></li>
					<li class="instagram"><a href="<?php the_field('instagram',$home->ID) ?>" target="_blank"></a></li>
					<li class="youtube"><a href="<?php the_field('youtube',$home->ID) ?>" target="_blank"></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

</div>

<?php wp_footer(); ?>

</body>

</html>