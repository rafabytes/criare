<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="smoothBody" class="site m-scene">

		<header id="masthead" class="site-header">

			<nav id="site-navigation" class="main-navigation">
				<div class="menu-toggle"></div>
				<?php
				wp_nav_menu(array(
					'theme_location'  => 'primary',
					'depth'           => 1, // 1 = no dropdowns, 2 = with dropdowns.
					'container'       => 'div',
					'container_id'    => 'main-menu',
					'menu_class'      => 'nav nav-pills nav-fill',
					'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
					'walker'          => new WP_Bootstrap_Navwalker(),
				));
				?>
			</nav><!-- #site-navigation -->

		</header><!-- #masthead -->

		<div id="content" class="site-content scene_element scene_element--fadeinleft">