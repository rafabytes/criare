<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Contato */
get_header();
?>

<div id="primary" class="content-area">
    <main id="contato" class="site-page">
        <section class="hero d-flex align-items-center" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>);">
            <a href="<?php echo home_url('news'); ?>" class="animsition-link arrow prev v-middle">News</a>
            <div class="container wow fadeInUp">
                <h1>vamos conversar?</h1>
            </div>
        </section>
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 map"></div>
                    <div class="col-md-4 form">
                        <p class="form-title">Se interessou em algum imóvel Criare? Escreve pra gente :)</p>
                        <form action="">
                            <label for="nome">Nome</label>
                            <input type="text">
                            <label for="nome">E-mail</label>
                            <input type="text">
                            <label for="nome">Telefone</label>
                            <input type="text" placeholder="DDD + Número">
                            <label for="">Mensagem</label>
                            <textarea name="" id=""></textarea>
                            <input type="submit" value="Enviar">
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="content-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h4>newsletter</h4>
                        <p>Cadastre-se e receba informações de lançamentos no seu email</p>
                        <form action="">
                            <div class="row">
                                <div class="col-12">
                                    <label for="">Digite seu email</label>
                                </div>
                                <div class="col-7 pr-0">
                                    <input type="email">
                                </div>
                                <div class="col-5">
                                    <input type="submit" value="cadastrar">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <h4>Construtora Criare</h4>
                        <ul class="menu">
                            <li><a href="#">Sobre</a></li>
                            <li><a href="#">Empreendimentos</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Trabalhe Conosco</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h4>Atendimento</h4>
                        <ul class="menu">
                            <li><a href="#">+55 81 3023-4567</a></li>
                            <li><a href="#">+55 81 99023-4567</a></li>
                            <li><a href="#">contato@construtoracriare.com.br</a></li>
                            <li><a href="#">Chat online</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<?php
get_footer();
