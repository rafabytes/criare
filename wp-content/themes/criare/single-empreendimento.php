<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="empreendimento" class="site-page">

		<section class="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>);">
			<a href="<?php echo home_url('sobre'); ?>" class="animsition-link arrow prev v-middle">Sobre</a>
		</section>

		<section class="informacoes">
			<div class="container">
				<div class="row">
					<div class="col-md-9 boxed">
						<div class="row main-box">
							<div class="col excerpt">
								<div class="breadcrumbs">
									<ul>
										<li><a href="#">Empreendimentos</a></li>
										<li><a href="#">Apartamento</a></li>
										<li><a href="#"><?php the_title() ?></a></li>
									</ul>
								</div>
								<div class="text-right">
									<a href="#" class="localizacao">Ver localização no mapa</a>
								</div>
								<h1>Reserva do Porto</h1>
								<p>O incentivo ao avanço tecnológico, assim como a determinação clara de objetivos acarreta um processo de reformulação e modernização de alternativas às soluções ortodoxas. No entanto, não podemos esquecer que a valorização de fatores subjetivos pode nos levar a considerar a reestruturação dos paradigmas corporativos.</p>
							</div>
							<div class="col dados px-0">
								<ul class="detalhes">
									<li class="area"><?php the_field('area_1') ?>m² a <?php the_field('area_2') ?>m²</li>
									<li class="dormitorio"><?php the_field('dormitorios') ?> dorms.</li>
									<li class="suite"><?php the_field('suites') ?> suítes</li>
									<li class="garagem"><?php the_field('vagas_de_garagem') ?> vagas de garagem</li>
								</ul>
								<ul class="contatos">
									<li><a href="<?php the_field('tabela_de_preco') ?>" class="preco">Tabela de PReços</a></li>
									<li><a href="<?php the_field('ebook') ?>" class="ebook">Baixar book</a></li>
									<li><a href="<?php echo home_url('tenho-interesse') ?>" class="interesse">Tenho interesse</a></li>
								</ul>
								<ul></ul>
							</div>
						</div>
						<div class="row">
							<div class="col ficha">
								<div class="row andamento">
									<div class="col">
										<div class="porcento">
											<?php the_field('projeto') ?>
										</div>
										<h5>Projeto</h5>
									</div>
									<div class="col">
										<div class="porcento">
											<?php the_field('fundacao') ?>
										</div>
										<h5>Fundação</h5>
									</div>
									<div class="col">
										<div class="porcento">
											<?php the_field('estrutura') ?>
										</div>
										<h5>Estrutura</h5>
									</div>
									<div class="col">
										<div class="porcento">
											<?php the_field('alvenaria') ?>
										</div>
										<h5>Alvenaria</h5>
									</div>
									<div class="col">
										<div class="porcento">
											<?php the_field('acabamento') ?>
										</div>
										<h5>Acabamento</h5>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<h5 class="section-limiter"><span>Ficha técnica</span></h5>
									</div>
								</div>
								<div class="row">
									<?php for ($i = 1; $i < 16; $i++) :
										if (get_field('item_descricao_' . $i)) : ?>
											<div class="col-md-3">
												<div class="item">
													<span class="icon"><img src="<?php the_field('item_icone_' . $i) ?>"></span>
													<?php the_field('item_descricao_' . $i) ?>
												</div>
											</div>
									<?php endif;
									endfor; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 meta">
						<h5>ARQUITETURA</h5>
						<p><?php the_field('arquitetura') ?></p>
						<h5>Interiores</h5>
						<p><?php the_field('interiores') ?></p>
						<h5>Paisagismo</h5>
						<p><?php the_field('paisagismo') ?></p>
						<h5>Lançamento</h5>
						<p><?php the_field('lancamento') ?></p>
						<h5>Previsão de entrega</h5>
						<p><?php the_field('previsao_de_entrega') ?></p>
					</div>
				</div>
			</div>
		</section>

		<section class="projeto">
			<div class="container">
				<div class="row">
					<h1>o projeto</h1>
				</div>
				<div class="row galerias">
					<div class="col-md-12">
						<div class="animated fadeInUp">
							<div class="filter d-md-none">
								<select id="finalidade" class="custom-select">
									<?php
									$fotos = acf_photo_gallery('fotos', get_the_ID());
									if (!empty($fotos)) : ?>
										<option value="#fotos">Fotos</option>
									<?php endif;
									$videos = acf_photo_gallery('videos', get_the_ID());
									if (!empty($videos)) : ?>
										<option value="#videos">Vídeos</option>
									<?php endif;
									$perspectivas = acf_photo_gallery('perspectivas', get_the_ID());
									if (!empty($perspectivas)) : ?>
										<option value="#perspectivas">Perspectivas</option>
									<?php endif;
									$vistas = acf_photo_gallery('vistas', get_the_ID());
									if (!empty($vistas)) : ?>
										<option value="#vistas">Vistas</option>
									<?php endif;
									$plantas = acf_photo_gallery('plantas', get_the_ID());
									if (!empty($plantas)) : ?>
										<option value="#plantas">Plantas</option>
									<?php endif;
									$obras = acf_photo_gallery('obras', get_the_ID());
									if (!empty($obras)) : ?>
										<option value="#obras">Obras</option>
									<?php endif; ?>
								</select>
							</div>
							<ul class="nav nav-tabs d-none d-md-block" id="myTab" role="tablist">
								<?php
								$active = true;
								if (!empty($fotos)) : ?>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" id="fotos-tab" data-toggle="tab" href="#fotos" role="tab" aria-controls="fotos" aria-selected="true">Fotos</a>
									</li>
								<?php endif;
								if (!empty($videos)) : ?>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" id="videos-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="videos" aria-selected="true">Vídeos</a>
									</li>
								<?php endif;
								if (!empty($perspectivas)) : ?>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" id="perspectivas-tab" data-toggle="tab" href="#perspectivas" role="tab" aria-controls="perspectivas" aria-selected="true">Perspectivas</a>
									</li>
								<?php endif;
								if (!empty($vistas)) : ?>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" id="vistas-tab" data-toggle="tab" href="#vistas" role="tab" aria-controls="vistas" aria-selected="true">Vistas</a>
									</li>
								<?php endif;
								if (!empty($plantas)) : ?>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" id="plantas-tab" data-toggle="tab" href="#plantas" role="tab" aria-controls="plantas" aria-selected="true">Plantas</a>
									</li>
								<?php endif;
								$obras = acf_photo_gallery('obra', get_the_ID());
								if (!empty($obras)) : ?>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" id="obras-tab" data-toggle="tab" href="#obras" role="tab" aria-controls="obras" aria-selected="true">Obras</a>
									</li>
								<?php endif;
								$localizacao = get_field('localizacao', get_the_ID());
								if (!empty($localizacao)) :
									$endereco = get_field('localizacao'); ?>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" id="mapa-tab" data-toggle="tab" href="#mapa" role="tab" aria-controls="mapa" aria-selected="true">Localização</a>
									</li>
									<li class="nav-item">
										<a class="nav-link <?php if ($active) : ?>active<?php endif;
																					$active = false; ?>" href="http://maps.google.com/maps?q=&layer=c&cbll=<?php echo esc_attr($endereco['lat']); ?>,<?php echo esc_attr($endereco['lng']); ?>" target="_blank">StreetView</a>
									</li>
								<?php endif; ?>
							</ul>

							<div class="tab-content" id="fotosTabContent">

								<?php
								$active = true;
								if (!empty($fotos)) : ?>
									<div class="tab-pane clearfix fade <?php if ($active) : ?>show active<?php endif;
																										$active = false; ?>" id="fotos" role="tabpanel" aria-labelledby="fotos-tab">

										<div class="owl-container d-none d-md-block">
											<div id="owlFotosMd" class="owl-carousel owl-galerias" data-items="1">
												<?php $i = 0;
												foreach ($fotos as $image) : ?>
													<?php if ($i % 7 == 0) : ?>
														<div>
														<?php endif ?>
														<div class="thumb <?php if (count($fotos) < 2) {
																				echo 'single';
																			} ?>" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
															<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="fotos" data-caption="<?php echo $image['caption'] ?>">
																<span class="zoom">Ver foto ampliada</span>
																<span class="caption"><?php echo $image['caption'] ?></span>
															</a>
														</div>
														<?php if (($i + 1) % 7 == 0 || ($i + 1) == count($fotos)) : ?>
														</div>
													<?php endif; ?>
												<?php $i++;
												endforeach; ?>
											</div>
											<?php if (count($fotos) > 7) : ?>
												<div class="navigation dark">
													<div class="navigation-arrows"></div>
													<div class="navigation-dots"></div>
												</div>
											<?php endif; ?>
										</div>

										<div class="owl-container d-md-none">
											<div id="owlFotosSm" class="owl-carousel owl-galerias" data-items="1">
												<?php foreach ($fotos as $image) : ?>
													<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
														<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="fotos" data-caption="<?php echo $image['caption'] ?>">
															<span class="zoom">Ver foto ampliada</span>
															<span class="caption"><?php echo $image['caption'] ?></span>
														</a>
													</div>
												<?php endforeach; ?>
											</div>
											<div class="navigation dark">
												<div class="navigation-arrows"></div>
												<div class="navigation-dots"></div>
											</div>
										</div>

									</div>

								<?php endif;
								if (!empty($videos)) : ?>
									<div class="tab-pane fade <?php if ($active) : ?>show active<?php endif;
																							$active = false; ?>" id="videos" role="tabpanel" aria-labelledby="videos-tab">
										<div class="owl-container d-none d-md-block">
											<div id="owlVideosMd" class="owl-carousel owl-galerias" data-items="1">
												<?php $i = 0;
												foreach ($videos as $image) : ?>
													<?php if ($i % 5 == 0) : ?>
														<div>
														<?php endif ?>
														<div class="thumb <?php if (count($videos) < 2) {
																				echo 'single';
																			} ?>" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
															<a href="<?php echo $image['url'] ?>" data-fancybox="videos" data-caption="<?php echo $image['caption'] ?>">
																<span class="zoom">Ver vídeo</span>
																<span class="caption"><?php echo $image['caption'] ?></span>
															</a>
														</div>
														<?php if (($i + 1) % 5 == 0 || ($i + 1) == count($videos)) : ?>
														</div>
													<?php endif; ?>
												<?php $i++;
												endforeach; ?>
											</div>
											<?php if (count($videos) > 5) : ?>
												<div class="navigation dark">
													<div class="navigation-arrows"></div>
													<div class="navigation-dots"></div>
												</div>
											<?php endif; ?>
										</div>

										<div class="owl-container d-md-none">
											<div id="owlVideosSm" class="owl-carousel owl-galerias" data-items="1">
												<?php foreach ($videos as $image) : ?>
													<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
														<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="videos" data-caption="<?php echo $image['caption'] ?>">
															<span class="zoom">Ver foto ampliada</span>
															<span class="caption"><?php echo $image['caption'] ?></span>
														</a>
													</div>
												<?php endforeach; ?>
											</div>
											<div class="navigation dark">
												<div class="navigation-arrows"></div>
												<div class="navigation-dots"></div>
											</div>
										</div>
									</div>

								<?php endif;
								if (!empty($perspectivas)) : ?>
									<div class="tab-pane fade <?php if ($active) : ?>show active<?php endif;
																							$active = false; ?>" id="perspectivas" role="tabpanel" aria-labelledby="perspectivas-tab">
										<div class="owl-container d-none d-md-block">
											<div id="owlPerspectivasMd" class="owl-carousel owl-galerias" data-items="1">
												<?php $i = 0;
												foreach ($perspectivas as $image) : ?>
													<?php if ($i % 5 == 0) : ?>
														<div>
														<?php endif ?>
														<div class="thumb <?php if (count($perspectivas) < 2) {
																				echo 'single';
																			} ?>" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
															<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="perspectivas" data-caption="<?php echo $image['caption'] ?>">
																<span class="zoom">Ver foto ampliada</span>
																<span class="caption"><?php echo $image['caption'] ?></span>
															</a>
														</div>
														<?php if (($i + 1) % 5 == 0 || ($i + 1) == count($perspectivas)) : ?>
														</div>
													<?php endif; ?>
												<?php $i++;
												endforeach; ?>
											</div>
											<?php if (count($perspectivas) > 5) : ?>
												<div class="navigation dark">
													<div class="navigation-arrows"></div>
													<div class="navigation-dots"></div>
												</div>
											<?php endif; ?>
										</div>

										<div class="owl-container d-md-none">
											<div id="owlPerspectivasSm" class="owl-carousel owl-galerias" data-items="1">
												<?php foreach ($perspectivas as $image) : ?>
													<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
														<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="perspectivas" data-caption="<?php echo $image['caption'] ?>">
															<span class="zoom">Ver foto ampliada</span>
															<span class="caption"><?php echo $image['caption'] ?></span>
														</a>
													</div>
												<?php endforeach; ?>
											</div>
											<div class="navigation dark">
												<div class="navigation-arrows"></div>
												<div class="navigation-dots"></div>
											</div>
										</div>
									</div>
								<?php endif;
								if (!empty($vistas)) : ?>
									<div class="tab-pane fade <?php if ($active) : ?>show active<?php endif;
																							$active = false; ?>" id="vistas" role="tabpanel" aria-labelledby="vistas-tab">
										<div class="owl-container d-none d-md-block">
											<div id="owlVistasMd" class="owl-carousel owl-galerias" data-items="1">
												<?php $i = 0;
												foreach ($vistas as $image) : ?>
													<?php if ($i % 5 == 0) : ?>
														<div>
														<?php endif ?>
														<div class="thumb <?php if (count($vistas) < 2) {
																				echo 'single';
																			} ?>" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
															<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="vistas" data-caption="<?php echo $image['caption'] ?>">
																<span class="zoom">Ver foto ampliada</span>
																<span class="caption"><?php echo $image['caption'] ?></span>
															</a>
														</div>
														<?php if (($i + 1) % 5 == 0 || ($i + 1) == count($vistas)) : ?>
														</div>
													<?php endif; ?>
												<?php $i++;
												endforeach; ?>
											</div>
											<?php if (count($vistas) > 5) : ?>
												<div class="navigation dark">
													<div class="navigation-arrows"></div>
													<div class="navigation-dots"></div>
												</div>
											<?php endif; ?>
										</div>

										<div class="owl-container d-md-none">
											<div id="owlVistasSm" class="owl-carousel owl-galerias" data-items="1">
												<?php foreach ($vistas as $image) : ?>
													<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
														<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="vistas" data-caption="<?php echo $image['caption'] ?>">
															<span class="zoom">Ver foto ampliada</span>
															<span class="caption"><?php echo $image['caption'] ?></span>
														</a>
													</div>
												<?php endforeach; ?>
											</div>
											<div class="navigation dark">
												<div class="navigation-arrows"></div>
												<div class="navigation-dots"></div>
											</div>
										</div>
									</div>
								<?php endif;
								if (!empty($plantas)) : ?>
									<div class="tab-pane fade <?php if ($active) : ?>show active<?php endif;
																							$active = false; ?>" id="plantas" role="tabpanel" aria-labelledby="plantas-tab">
										<div class="owl-container d-none d-md-block">
											<div id="owlPlantasMd" class="owl-carousel owl-galerias" data-items="1">
												<?php $i = 0;
												foreach ($plantas as $image) : ?>
													<?php if ($i % 5 == 0) : ?>
														<div>
														<?php endif ?>
														<div class="thumb <?php if (count($plantas) < 2) {
																				echo 'single';
																			} ?>" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
															<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="plantas" data-caption="<?php echo $image['caption'] ?>">
																<span class="zoom">Ver foto ampliada</span>
																<span class="caption"><?php echo $image['caption'] ?></span>
															</a>
														</div>
														<?php if (($i + 1) % 5 == 0 || ($i + 1) == count($plantas)) : ?>
														</div>
													<?php endif; ?>
												<?php $i++;
												endforeach; ?>
											</div>
											<?php if (count($plantas) > 5) : ?>
												<div class="navigation dark">
													<div class="navigation-arrows"></div>
													<div class="navigation-dots"></div>
												</div>
											<?php endif; ?>
										</div>

										<div class="owl-container d-md-none">
											<div id="owlPlantasSm" class="owl-carousel owl-galerias" data-items="1">
												<?php foreach ($plantas as $image) : ?>
													<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
														<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="plantas" data-caption="<?php echo $image['caption'] ?>">
															<span class="zoom">Ver foto ampliada</span>
															<span class="caption"><?php echo $image['caption'] ?></span>
														</a>
													</div>
												<?php endforeach; ?>
											</div>
											<div class="navigation dark">
												<div class="navigation-arrows"></div>
												<div class="navigation-dots"></div>
											</div>
										</div>
									</div>
								<?php endif;
								if (!empty($obras)) : ?>
									<div class="tab-pane fade <?php if ($active) : ?>show active<?php endif;
																							$active = false; ?>" id="obras" role="tabpanel" aria-labelledby="obras-tab">
										<div class="owl-container d-none d-md-block">
											<div id="owlObrasMd" class="owl-carousel owl-galerias" data-items="1">
												<?php $i = 0;
												foreach ($obras as $image) : ?>
													<?php if ($i % 5 == 0) : ?>
														<div>
														<?php endif ?>
														<div class="thumb <?php if (count($obras) < 2) {
																				echo 'single';
																			} ?>" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
															<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="obras" data-caption="<?php echo $image['caption'] ?>">
																<span class="zoom">Ver foto ampliada</span>
																<span class="caption"><?php echo $image['caption'] ?></span>
															</a>
														</div>
														<?php if (($i + 1) % 5 == 0 || ($i + 1) == count($obras)) : ?>
														</div>
													<?php endif; ?>
												<?php $i++;
												endforeach; ?>
											</div>
											<?php if (count($obras) > 5) : ?>
												<div class="navigation dark">
													<div class="navigation-arrows"></div>
													<div class="navigation-dots"></div>
												</div>
											<?php endif; ?>
										</div>

										<div class="owl-container d-md-none">
											<div id="owlObrasSm" class="owl-carousel owl-galerias" data-items="1">
												<?php foreach ($obras as $image) : ?>
													<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'], 530, 465) ?>)">
														<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="obras" data-caption="<?php echo $image['caption'] ?>">
															<span class="zoom">Ver foto ampliada</span>
															<span class="caption"><?php echo $image['caption'] ?></span>
														</a>
													</div>
												<?php endforeach; ?>
											</div>
											<div class="navigation dark">
												<div class="navigation-arrows"></div>
												<div class="navigation-dots"></div>
											</div>
										</div>
									</div>
								<?php endif;
								if (!empty($localizacao)) : ?>
									<div class="tab-pane fade <?php if ($active) : ?>show active<?php endif;
																							$active = false; ?>" id="mapa" role="tabpanel" aria-labelledby="mapa-tab">
										<div id="gmaps" data-lat="<?php echo esc_attr($endereco['lat']); ?>" data-lng="<?php echo esc_attr($endereco['lng']); ?>"></div>
									</div>
								<?php endif; ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="tour d-flex text-center align-items-end" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/sobre-cover.jpg);">
			<h1>tour virtual 360º</h1>
		</section>

		<section class="slides">
			<div class="container">
				<div class="owl-container owl-slides d-none d-md-block">
					<div id="owlSlides" class="owl-carousel" data-items="1">
						<div class="item">
							<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-01.jpg);"></div>
							<div class="box">
								<h3>1 - Inspire. respire. Recomece em porto.</h3>
								<p>O incentivo ao avanço tecnológico, assim como a determinação clara de objetivos acarreta um processo de reformulação e modernização de alternativas às soluções ortodoxas. No entanto, não podemos considerar a reestruturação dos paradigmas corporativos. </p>
							</div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-01.jpg);"></div>
							<div class="box">
								<h3>2 - Inspire. respire. Recomece em porto.</h3>
								<p>O incentivo ao avanço tecnológico, assim como a determinação clara de objetivos acarreta um processo de reformulação e modernização de alternativas às soluções ortodoxas. No entanto, não podemos considerar a reestruturação dos paradigmas corporativos. </p>
							</div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-01.jpg);"></div>
							<div class="box">
								<h3>3 - Inspire. respire. Recomece em porto.</h3>
								<p>O incentivo ao avanço tecnológico, assim como a determinação clara de objetivos acarreta um processo de reformulação e modernização de alternativas às soluções ortodoxas. No entanto, não podemos considerar a reestruturação dos paradigmas corporativos. </p>
							</div>
						</div>
					</div>
					<div class="navigation">
						<div class="navigation-arrows"></div>
						<div class="navigation-dots"></div>
					</div>
				</div>
				<div class="licensas">
					<p><?php the_field('registro') ?></p>
					<a href="<?php the_field('licensa') ?>">Ver licenças</a>
				</div>
			</div>
		</section>


	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
