<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Home */
get_header();
?>

<div id="primary" class="content-area">

    <main id="home" class="site-main">

        <section id="hero" class="hero site-main d-flex align-items-center">
            <div class="img-filter"></div>
            <div class="container wow fadeInRight">
                <div class="row">
                    <div class="col">
                        <h1><?php the_field('titulo') ?></span></h1>
                        <!-- <h1>Muito prazer, somos a <span>Criare.</span></h1> -->
                        <p><?php the_field('subtitulo') ?></p>
                        <!-- <p>Queremos te ajudar a construir a sua história. </p> -->
                        <a href="<?php the_field('link_do_botao') ?>" class="btn clean"><?php the_field('texto_do_botao') ?></a>
                        <!-- <a href="<?php echo home_url('/sobre'); ?>" class="btn clean">conheça</a> -->
                    </div>
                </div>
            </div>
            <a href="<?php echo home_url('/sobre'); ?>" class="animsition-link arrow next v-middle">Sobre</a>
        </section>

        <footer id="homeFooter" class="site-footer">
            <div class="container px-0">
                <div class="row align-items-center">
                    <div class="logo col">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" alt="Criare">
                    </div>
                    <div class="copyright col text-center">@2021 Criare - Todos os Direitos Reservados</div>
                    <div class="site-info col text-right">
                        <a href="tel:<?php the_field('telefone') ?>" class="fone"><?php the_field('telefone') ?></a>
                        <ul class="social">
                            <li class="facebook"><a href="<?php the_field('facebook') ?>" target="_blank"></a></li>
                            <li class="instagram"><a href="<?php the_field('instagram') ?>" target="_blank"></a></li>
                            <li class="youtube"><a href="<?php the_field('youtube') ?>" target="_blank"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

    </main>

</div>

</div><!-- #content -->

</div>

<?php wp_footer(); ?>

</body>

</html>