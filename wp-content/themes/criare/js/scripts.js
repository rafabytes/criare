jQuery(document).ready(function ($) {

  new WOW().init();

  if ($('.owl-imovel').length) {
    $('.owl-imovel').owlCarousel({
      items: 1,
      smartSpeed: 800,
      nav: false,
      dots: true
    });
  }

  //start owl
  var rootClassName = 'owl-container';
  var navContainerClassName = '.navigation-arrows';
  var dotsContainerClassName = '.navigation-dots';
  $('.owl-carousel').each(function (index, item) {
    var items = $(this).data('items');
    var autoplay = $(this).data('autoplay');
    var loop = false;
    var mouseDrag = true;
    if (autoplay > 0) { loop = true; mouseDrag = false }
    var rootIdClassName = 'owl--' + rootClassName + '--' + index;
    $(item).closest('.' + rootClassName).addClass(rootIdClassName);
    $(item).owlCarousel({
      items: 1,
      autoplay: autoplay,
      loop: loop,
      margin: 30,
      nav: true,
      mouseDrag: mouseDrag,
      animateOut: 'fadeOut',
      smartSpeed: 300,
      onInitialized: attNavigation,
      navContainer: '.' + rootIdClassName + ' ' + navContainerClassName,
      dotsContainer: '.' + rootIdClassName + ' ' + dotsContainerClassName,
      responsive: {
        992: {
          items: items,
          slideBy: items
        }
      }
    });
  });

  function attNavigation() {
    $('.owl-container').each(function () {
      var dots = $(this).find('.navigation-dots');
      $(this).find('.owl-prev').after(dots);
    });
  }

  $('.custom-select').selectpicker();

  // 'use strict';
  // var options = {
  //   prefetch: true,
  //   blacklist: '.no-smoothState', //links with this class are ignored, like my open menu button
  //   scroll: true,
  //   cacheLength: 2,
  //   onStart: {
  //     duration: 500, // Duration of our animation, if animations are longer than this, it causes problems
  //     render: function ($container) {

  //       // Add your CSS animation reversing class

  //       $container.addClass('is-exiting');


  //       // Restart your animation. 
  //       //If you looked at the authors demo, it uses .toggleAnimationClass(), which is outdated now
  //       //The Smoothstate demo on the authors site is outdated, but his Github is up to date and uses this method now.
  //       smoothState.restartCSSAnimations();
  //     }
  //   },
  //   onReady: {
  //     duration: 0,
  //     render: function ($container, $newContent) {
  //       // Remove your CSS animation reversing class

  //       $container.removeClass('is-exiting');

  //       // Inject the new content
  //       $container.html($newContent);

  //     }
  //   }
  // },
  //   smoothState = $('#smoothBody').smoothState(options).data('smoothState');

});