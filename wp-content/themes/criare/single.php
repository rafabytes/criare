<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="news" class="site-content news-artigo">

		<article>
			<div class="article-hero" style="background-image: url(<?php echo get_the_post_thumbnail_url() ?>);">
			</div>
			<div class="article-body">
				<div class="container pr-0">
					<div class="row">
						<div class="col content">
							<h1><?php the_title() ?></h1>
							<?php the_content() ?>
						</div>
						<div class="col sidebar">
							<div class="meta">
								<div class="data"><?php the_time('d/m/Y') ?> - <?php the_time() ?></div>
								<div class="categoria">Em <a href="#">Tecnologia</a></div>
								<div class="tags">
									<p>Tags</p>
									<ul>
										<li><a href="#">tecnologia</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>

		<section class="noticias">

			<h1 class="section-title">Relacionados</h1>

			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<article>
							<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-02.jpg);"></div>
							<div class="meta">
								<div class="tag">Tecnologia</div>
								<h1>Título do artigo neste local em até duas linhas de texto</h1>
							</div>
						</article>
					</div>
					<div class="col-md-6">
						<article>
							<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-02.jpg);"></div>
							<div class="meta">
								<div class="tag">Tecnologia</div>
								<h1>Título do artigo neste local em até duas linhas de texto</h1>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
