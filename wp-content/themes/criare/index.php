<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

get_header();

?>


<div id="primary" class="content-area">
	<main id="news" class="site-page news-lista">

		<section class="page-header">
			<a href="<?php echo home_url(''); ?>" class="animsition-link arrow prev v-middle">Sobre</a>
			<div class="container">
				<h1><?php the_field('titulo_1') ?></h1>
			</div>
			<a href="<?php echo home_url('/empreendimentos'); ?>" class="animsition-link arrow next v-middle">Empreendimentos</a>
		</section>

		<section class="noticias">
			<div class="container">
				<div class="row">
					<?php
					$imoveis = new WP_Query(
						array(
							'post_type' => 'post',
							'posts_per_page' => -1
						)
					);
					while ($imoveis->have_posts()) :  $imoveis->the_post();
					?>
						<a href="<?php the_permalink() ?>" class="col-md-6">
							<article>
								<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-02.jpg);"></div>
								<div class="meta">
									<div class="tag">
										<?php
										$categories = get_the_category();
										foreach ($categories as $category) { ?>
											<?php echo $category->name; ?>
										<?php } ?>
									</div>
									<h1><?php the_title() ?></h1>
								</div>
							</article>
						</a>
					<?php
					endwhile;
					wp_reset_postdata();
					?>
				</div>
			</div>
		</section>

		<section class="page-header">
			<div class="container">
				<h1><?php the_field('titulo_2') ?></h1>
			</div>
		</section>

		<section class="noticias">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<article>
							<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-02.jpg);"></div>
							<div class="meta">
								<div class="tag">Tecnologia</div>
								<h1>Título do artigo neste local em até duas linhas de texto</h1>
							</div>
						</article>
					</div>
					<div class="col-md-6">
						<article>
							<div class="img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/empreendimento-02.jpg);"></div>
							<div class="meta">
								<div class="tag">Tecnologia</div>
								<h1>Título do artigo neste local em até duas linhas de texto</h1>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>

	</main>
</div>

<?php
get_footer();
