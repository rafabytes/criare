<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Sobre */
get_header();
?>

<div id="primary" class="content-area">

    <main id="sobre" class="site-main">

        <section id="hero" class="hero d-flex align-items-center">
            <div class="img-filter"></div>
            <a href="<?php echo home_url(''); ?>" class="animsition-link arrow prev v-middle">Sobre</a>
            <div class="container wow fadeInUp">
                <div class="row align-items-center">
                    <div class="col-md-7">
                        <h1><?php the_field('titulo_1') ?></h1>
                        <p><?php the_field('subtitulo_1') ?></p>
                    </div>
                    <div class="offset-md-1 col-md-4">
                        <div class="opcoes">
                            <a href="<?php echo home_url('agende-uma-visita') ?>" class="opcao">Agende uma visita</a>
                            <a href="<?php echo home_url('negocie-seu-terreno') ?>" class="opcao">negocie seu terreno</a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="<?php echo home_url('/empreendimentos'); ?>" class="animsition-link arrow next v-middle">Empreendimentos</a>
        </section>

        <section id="historia">
            <div class="container boxed wow fadeInUp">
                <div class="row">
                    <div class="col-md-6">
                        <img src="<?php the_field('imagem_2') ?>" alt="Criare">
                    </div>
                    <div class="col-md-6">
                        <h2><?php the_field('titulo_2') ?></h2>
                        <p><?php the_field('texto_2') ?></p>
                    </div>
                </div>
            </div>
            <div class="box-footer numeros">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h5><span>criare em números</span></h5>
                        </div>
                        <div class="col-md-3">
                            <div class="numero"><?php the_field('numero_1') ?></div>
                            <p><?php the_field('numero_descricao_1') ?></p>
                        </div>
                        <div class="col-md-3">
                            <div class="numero"><?php the_field('numero_2') ?></div>
                            <p><?php the_field('numero_descricao_2') ?></p>
                        </div>
                        <div class="col-md-3">
                            <div class="numero"><?php the_field('numero_3') ?></div>
                            <p><?php the_field('numero_descricao_3') ?></p>
                        </div>
                        <div class="col-md-3">
                            <div class="numero"><?php the_field('numero_4') ?></div>
                            <p><?php the_field('numero_descricao_4') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="missao">
            <div class="container boxed wow fadeInUp">
                <div class="row">
                    <div class="col-12">
                        <h2><?php the_field('titulo_3') ?></h2>
                    </div>
                    <div class="col-md-6">
                        <p><?php the_field('texto_esquerda') ?></p>
                    </div>
                    <div class="col-md-6">
                        <p><?php the_field('texto_direita') ?></p>
                    </div>
                </div>
            </div>
            <div class="box-footer premios">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <h5><span>Prêmios recebidos</span></h5>
                        </div>

                        <?php
                        $premios = acf_photo_gallery('premios', get_the_ID());
                        foreach ($premios as $image) :
                        ?>
                            <div class="col-md-3">
                                <img src="<?php echo $image['full_image_url'] ?>">
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>

    </main>

</div>


<?php
get_footer();
